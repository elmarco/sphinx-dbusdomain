Testing D-Bus domain
====================

.. contents::
   :local:
   :depth: 3

.. dbus:interface:: org.freedesktop.DBus

   Test here interface.

   .. dbus:method:: StartServiceByName

      Test here

      :arg s name: Name of the service to start
      :arg u flags: Flags (currently not used)
      :ret u value: Return value

      Tries to launch the executable associated with a name (service
      activation), as an explicit request. This is an alternative to relying on
      auto-starting. For more information on how services are activated and the
      difference between auto-starting and explicit activation, see the section
      called “Message Bus Starting Services (Activation)”.

   .. dbus:method:: NoReply
      :noreply:

   .. dbus:signal:: NameLost

      :arg s name: Name which was lost

      This signal is sent to a specific application when it loses ownership of a name.

   .. dbus:property:: Features
      :type: as

      Cross-reference :dbus:iface:`org.freedesktop.DBus2` and
      :dbus:meth:`~org.freedesktop.DBus2.Foo`.

   .. dbus:property:: Interfaces
      :type: as
      :readonly:
      :deprecated:
      :emits-changed: const

      Let's reference :dbus:meth:`NoReply`.

.. dbus:interface:: org.freedesktop.DBus2

   An interface.

   .. dbus:method:: Foo

   The method Foo().
