import alabaster
import sphinx_rtd_theme

project = "sphinx-dbusdomain"
copyright = "2021, Red Hat Inc."
release = "0.0"
version = "0.0.1"
needs_sphinx = "1.0"
extensions = [
    "sphinx.ext.intersphinx",
    "dbusdomain",
    "sphinx_rtd_theme",
]

master_doc = "index"
html_theme = "sphinx_rtd_theme"
html_sidebars = {
    "**": [],
}
html_theme_options = {
    #    'description': 'Describe DBus schema',
}
intersphinx_mapping = {
    "python": ("https://docs.python.org/3", None),
}
dbus_index_common_prefix = ["org.freedesktop."]
